import { Component } from '@angular/core';

import { LocalDataSource } from 'ng2-smart-table';

import { SmartTableService } from '../../../@core/data/smart-table.service';
import 'style-loader!angular2-toaster/toaster.css';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-answered',
  styleUrls: ['./answered.component.scss'],
  templateUrl: './answered.component.html',
})
export class AnsweredComponent {
  selectedId:number[]=[];
  patientId:number;
  selected:any;
  isSelected:boolean=false;
  settings = {
    mode: 'external', 
    actions: {
      add: false,
      edit: false,
      delete: false,
      position:'right',
      custom: [
        {
          name: 'details',
          title: 'Details'
        },
      ]
    },
    
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    selectMode: 'multi',
    columns: {
      
      id: {
        title: 'ID',
        type: 'number',
      },
      patient_id: {
        title: 'PAtient ID',
        type: 'number',
      },
      country: {
        title: 'Country',
        type: 'string',
      },
      specialization: {
        title: 'Specialization',
        type: 'string',
      },
      date_time: {
        title: 'Date Time',
        type: 'string',
      },
      doctor_id: {
        title: 'Doctor ID',
        type: 'number',
      },
      rate: {
        title: 'Rate',
        type: 'number',
      },
      payment_method: {
        title: 'Payment Method',
        type: 'string',
      },
      type: {
        title: 'Type',
        type: 'string',
      },
    },
   
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private service: SmartTableService, private router:Router) {
    const data = this.service.getData();
    this.source.load(data);
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
  userRowSelect(event){

    this.selected=event;
    //console.log(this.selected)
    if (event.selected.length !=0){
      this.isSelected=true;
      console.log(event)
      
      this.selectedId.push(event.data.id)
      console.log(this.selectedId)
    }
    else{
      this.isSelected=false;
    }
  }
  onRefund(){
      alert('this is details button should route to details of doctor whos id is ' +this.patientId +' page')
      var myurl = `/pages/consultation/consultationRefund/${this.selectedId}`;
      //console.log(myurl)
      this.router.navigateByUrl(myurl)
  }
  onCustomAction(event){
    if (event.action == 'details'){
      this.patientId= event.data.id;
      alert('this is details button should route to details of doctor whos id is ' +this.patientId +' page')
      let myurl = `/pages/consultations/answered/${this.patientId}`;
      //console.log(myurl)
      this.router.navigateByUrl(myurl)
    }
    
  }
}
