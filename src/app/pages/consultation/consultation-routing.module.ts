import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConsultationComponent } from './consultation.component';
import { UnpaidComponent } from './unpaid/unpaid.component';
import { UnAnsweredComponent } from './un-answered/un-answered.component';
import { ReportedComponent } from './reported/reported.component';
import { NotMySpecificationComponent } from './not-my-specification/not-my-specification.component';
import { InsufficientInformationComponent } from './insufficient-information/insufficient-information.component';
import { AnsweredComponent } from './answered/answered.component';
import { RefundComponent } from './refund/refund.component';


const routes: Routes = [{
  path: '',
  component: ConsultationComponent,
  children: [
  {
    path: 'answered',
    component: AnsweredComponent,
  }, {
    path: 'insufficient-information',
    component: InsufficientInformationComponent,
  },
  {
    path: 'not-my-specification',
    component: NotMySpecificationComponent,
  }, {
    path: 'reported',
    component: ReportedComponent,
  },
  {
    path: 'un-answered',
    component: UnAnsweredComponent,
  }, {
    path: 'unpaid',
    component: UnpaidComponent,
  },
  {
    path:'answered/:id',
    component:RefundComponent
  },
  {
    path:'consultationRefund/:id',
    component:RefundComponent
  },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsultationRoutingModule { }

export const routedComponents = [
  ConsultationComponent,
  AnsweredComponent,
  InsufficientInformationComponent,
  NotMySpecificationComponent,
  ReportedComponent,
  UnAnsweredComponent,
  UnpaidComponent,
  RefundComponent
];
