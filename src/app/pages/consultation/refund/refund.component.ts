import { Component} from "@angular/core";
//import { ActivatedRoute, Router } from "@angular/router";
import { LocalDataSource } from 'ng2-smart-table';
import { AnsweredComponent } from "../answered/answered.component";
//import { SmartTableService } from '../../../@core/data/smart-table.service';
//import{AnsweredComponent} from '../answered/answered.component';
//import { from } from "rxjs";
@Component({
    templateUrl:'./refund.component.html',
    styleUrls:['./refund.component.css']
})
export class RefundComponent{
    //answered=AnsweredComponent;
    
    //selectedId:number[]=[];
    selected:any;
    isSelected:boolean=false;
    settings = {
      mode: 'external', 
      actions: {
        add: false,
        edit: false,
        delete: false,
        position:'right',
        custom: [
          {
            name: 'refund',
            title: 'Refund'
          },
        ]
      },
      selectMode: 'multi',
      columns: {
        
        id: {
          title: 'ID',
          type: 'number',
        },
        name:{
            title:'Name',
            type:'string',
        },
        country: {
          title: 'Country',
          type: 'string',
        },
        specialization: {
          title: 'Specialization',
          type: 'string',
        },
        sub_spec: {
            title: 'Sub Spec.',
            type: 'string',
        },
        date_time: {
          title: 'Date Time',
          type: 'string',
        },
        degree: {
          title: 'Degree',
          type: 'number',
        },
        type: {
            title: 'Type',
            type: 'string',
        },
        rate: {
          title: 'Rate',
          type: 'number',
        },
        age: {
            title: 'Age',
            type: 'number',
        },
        answerConsult: {
            title: 'Answered General Consultations',
            type: 'number',
        },
        unanswerConsult: {
            title: 'UnAnswered Direct Consultations',
            type: 'number',
        },
        total_balace: {
          title: 'Total Balance',
          type: 'number',
        },
        current_balace: {
            title: 'Current Balance',
            type: 'number',
        },
        contacts: {
            title: 'Contacts',
            type: 'string',
        },
        
      },
     
    };
    source: LocalDataSource = new LocalDataSource();

    constructor(
      
      //private service: SmartTableService,
      //private router:Router,
      //private activatedRoute: ActivatedRoute
       ) {
         
         
      //this.selectedId.push(activatedRoute.snapshot.params.id)
      //console.log(this.selectedId)
      //const data = this.service.getData();
      //this.source.load(data);
    }
}