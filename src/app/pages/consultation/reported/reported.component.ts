import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { SmartTableService } from '../../../@core/data/smart-table.service';
import 'style-loader!angular2-toaster/toaster.css';

@Component({
  selector: 'ngx-reported',
  styleUrls: ['./reported.component.scss'],
  templateUrl: './reported.component.html',
})
export class ReportedComponent {
  selected:any;
  isSelected:boolean=false;
  settings = {
    mode: 'external', 
    actions: {
      add: false,
      edit: false,
      delete: false,
      position:'right',
      custom: [
        {
          name: 'details',
          title: 'Details'
        },
      ]
    },
    
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    selectMode: 'multi',
    columns: {
      
      id: {
        title: 'ID',
        type: 'number',
      },
      patient_id: {
        title: 'PAtient ID',
        type: 'number',
      },
      country: {
        title: 'Country',
        type: 'string',
      },
      specialization: {
        title: 'Specialization',
        type: 'string',
      },
      date_time: {
        title: 'Date Time',
        type: 'string',
      },
      doctor_id: {
        title: 'Doctor ID',
        type: 'number',
      },
      rate: {
        title: 'Rate',
        type: 'number',
      },
      payment_method: {
        title: 'Payment Method',
        type: 'string',
      },
      type: {
        title: 'Type',
        type: 'string',
      },
      doctor_comment: {
        title: "Doctor's Comment",
        type: 'string',
      },
    },
   
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private service: SmartTableService) {
    const data = this.service.getData();
    this.source.load(data);
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
  userRowSelect(event){

    this.selected=event;
    console.log(this.selected)
    if (event.selected.length !=0){
      this.isSelected=true;
    }
    else{
      this.isSelected=false;
    }
  }
}
