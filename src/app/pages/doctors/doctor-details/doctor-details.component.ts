import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { SmartTableService } from '../../../@core/data/smart-table.service';
@Component({
    templateUrl:'./doctor-details.component.html',
    styleUrls:['./doctor-details.component.scss']
})
export class DoctorDetailsComponent implements OnInit{
  isActivate:boolean=false;
  isDeactivate:boolean=false;
  
    id:number
    settings = {
        mode: 'external', 
        actions: {
          position:'right',
        },
        edit: {
          editButtonContent: '<i class="nb-edit"></i>',
          saveButtonContent: '<i class="nb-checkmark"></i>',
          cancelButtonContent: '<i class="nb-close"></i>',
        },
        delete: {
          deleteButtonContent: '<i class="nb-trash"></i>',
          confirmDelete: true,
        },
        selectMode: 'multi',
        columns: {
          
         
          firstName: {
            title: 'Name',
            type: 'string',
          },
          country: {
            title: 'Address',
            type: 'string',
          },
         contacts: {
            title: 'Phone',
            type: 'string',
          },
         
        },
       
      };
    constructor(private activatedRoute: ActivatedRoute , private router:Router) {
      console.log(router.url)
      debugger
      this.id=activatedRoute.snapshot.params.id
        console.log(this.id); // Print the parameter to the console. 
    
      }

    ngOnInit(){
      debugger
     
      if(this.router.url == `/pages/doctors/update-request/${this.id}`){
        this.isActivate=true;
        this.isDeactivate=true;

      }
      else if(this.router.url == `/pages/doctors/activate/${this.id}`){
        this.isActivate=false;
        this.isDeactivate=true;
      }
      else if(this.router.url == `/pages/doctors/deactivate/${this.id}`){
        this.isActivate=true;
        this.isDeactivate=false;
      }
      else if(this.router.url == `/pages/doctors/under-registeration/${this.id}`){
        this.isActivate=true;
        this.isDeactivate=false;
      }

    }
}