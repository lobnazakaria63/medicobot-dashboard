import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DoctorsComponent } from './doctors.component';
import { UnderRegisterationComponent } from './under-registeration/under-registeration.component';
import { ActivatedComponent } from './activated/activated.component';
import { DeactivatedComponent } from './deactivated/deactivated.component';
import {UpdateRequestComponent} from './update-request/update-request.component';
import { DoctorDetailsComponent } from './doctor-details/doctor-details.component';
const routes: Routes = [{
  path: '',
  component: DoctorsComponent,
  children: [{
    path: 'under-registeration',
    component: UnderRegisterationComponent,
  }, {
    path: 'activated',
    component: ActivatedComponent,
  },
  {
    path:'deactivated',
    component:DeactivatedComponent,
  },
  {
    path:'update-request',
    component:UpdateRequestComponent,
  },
  {
    path:'under-registerarion/:id',
    component:DoctorDetailsComponent
  },
  {
    path:'activated/:id',
    component:DoctorDetailsComponent
  },
  {
    path:'deactivated/:id',
    component:DoctorDetailsComponent
  },
  {
    path:'update-request/:id',
    component:DoctorDetailsComponent
  },
],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class DoctorsRoutingModule {

}

export const routedComponents = [
  DoctorsComponent,
  UnderRegisterationComponent,
  ActivatedComponent,
  DeactivatedComponent,
  UpdateRequestComponent,
  DoctorDetailsComponent
];
