import { Component } from '@angular/core';

@Component({
  selector: 'ngx-doctor-elements',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class DoctorsComponent {
}
