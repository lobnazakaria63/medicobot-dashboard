import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { SmartTableService } from '../../../@core/data/smart-table.service';
import { Router } from '@angular/router';
@Component({
  selector: 'ngx-under-registeration',
  styleUrls: ['./under-registeration.component.scss'],
  templateUrl: './under-registeration.component.html',
})
export class UnderRegisterationComponent {
  doctorId:number;
  selected:any;
  isSelected:boolean=false;
  settings = {
    mode: 'external', 
    actions: {
      width:150,
      add: false,
      edit: false,
      delete: false,
     position:'right',
      custom: [
          
          {
              name: 'details',
              title: 'Details'
          },
         
      ]
    },
    
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    selectMode: 'multi',
    columns: {
      
      id: {
        title: 'ID',
        type: 'number',
      },
      firstName: {
        title: 'Name',
        type: 'string',
      },
      country: {
        title: 'Country',
        type: 'string',
      },
      specialization: {
        title: 'Specialization',
        type: 'string',
      },
      sub_spec: {
        title: 'Sub Spec.',
        type: 'string',
      },
      degree: {
        title: 'Degree',
        type: 'string',
      },
      type: {
        title: 'Type',
        type: 'string',
      },
      rate: {
        title: 'Rate',
        type: 'string',
      },
      age: {
        title: 'Age',
        type: 'number',
      },
      answered_G_C: {
        title: 'Answered General Consultation',
        type: 'string',
      },
      total_balance: {
        title: 'Total Balance',
        type: 'string',
      },
      current_balance: {
        title: 'Current Balance',
        type: 'string',
      },
      contacts: {
        title: 'Contacts',
        type: 'string',
      },
     
    },
   
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private service: SmartTableService, private router:Router) {
    const data = this.service.getData();
    this.source.load(data);
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
  userRowSelect(event){

    this.selected=event;
    console.log(this.selected)
    if (event.selected.length !=0){
      this.isSelected=true;
    }
    else{
      this.isSelected=false;
    }
  }
  onCustomAction(event){
    if (event.action == 'details'){
      this.doctorId= event.data.id;
      alert('this is details button should route to details of doctor whos id is ' +this.doctorId +' page')
      var myurl = `/pages/doctors/under-registerarion/${this.doctorId}`;
      console.log(myurl)
      this.router.navigateByUrl(myurl)
    }
    else if(event.action == 'activate'){
      alert('this is activate button should activate doctor')
    }
    else if (event.action == 'sendMsg'){
      alert('this is sendMsg buttonshould send message to doctor')
    }
    else{
      alert('this is delete buttonshould delete doctor record')
    }
  }
}
