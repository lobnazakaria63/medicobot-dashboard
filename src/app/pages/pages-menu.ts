import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
 
  {
    title: 'IoT Dashboard',
    icon: 'nb-home',
    link: '/pages/iot-dashboard',
    home: true,
  },
  {
    title: 'FEATURES',
    group: true,
  },
  {
    title: 'UI Features',
    icon: 'nb-keypad',
    link: '/pages/ui-features',
    children: [
      {
        title: 'Buttons',
        link: '/pages/ui-features/buttons',
      },
      {
        title: 'Grid',
        link: '/pages/ui-features/grid',
      },
      {
        title: 'Icons',
        link: '/pages/ui-features/icons',
      },
      {
        title: 'Modals',
        link: '/pages/ui-features/modals',
      },
      {
        title: 'Popovers',
        link: '/pages/ui-features/popovers',
      },
      {
        title: 'Typography',
        link: '/pages/ui-features/typography',
      },
      {
        title: 'Animated Searches',
        link: '/pages/ui-features/search-fields',
      },
      {
        title: 'Tabs',
        link: '/pages/ui-features/tabs',
      },
    ],
  },
  {
    title: 'Doctors',
    icon: 'fa fa-user-md',
    children: [
      {
        title: 'Under Registration',
        link: '/pages/doctors/under-registeration',
      },
      {
        title: 'Activated',
        link: '/pages/doctors/activated',
      },
      {
        title: 'Deactivated',
        link: '/pages/doctors/deactivated',
      },
      {
        title: 'Update Requests',
        link: '/pages/doctors/update-request',
      },
    ],
  },
  {
    title: 'Consultation',
    icon: 'nb-compose',
    children: [
      {
        title: 'Unpaid',
        link: '/pages/consultation/unpaid',
      }, {
        title: 'Answered',
        link: '/pages/consultation/answered',
      },
      {
        title: 'Un-Answered',
        link: '/pages/consultation/un-answered',
      }, {
        title: 'Reported',
        link: '/pages/consultation/reported',
      },
      {
        title: 'Not My Specification',
        link: '/pages/consultation/not-my-specification',
      }, {
        title: 'Insufficient Information',
        link: '/pages/consultation/insufficient-information',
      },
    ],
  },
  {
    title: 'Maps',
    icon: 'nb-location',
    children: [
      {
        title: 'Google Maps',
        link: '/pages/maps/gmaps',
      },
      {
        title: 'Leaflet Maps',
        link: '/pages/maps/leaflet',
      },
      {
        title: 'Bubble Maps',
        link: '/pages/maps/bubble',
      },
      {
        title: 'Search Maps',
        link: '/pages/maps/searchmap',
      },
    ],
  },
  {
    title: 'Charts',
    icon: 'nb-bar-chart',
    children: [
      {
        title: 'Echarts',
        link: '/pages/charts/echarts',
      },
      {
        title: 'Charts.js',
        link: '/pages/charts/chartjs',
      },
      {
        title: 'D3',
        link: '/pages/charts/d3',
      },
    ],
  },
  {
    title: 'Specialization',
    icon: 'nb-title',
    children: [
      {
        title: 'Doctors',
        link: '/pages/specialization/doctors',
      },
      {
        title: 'Patients',
        link: '/pages/specialization/patients',
      },
    ],
  },
  {
    title: 'Tables',
    icon: 'nb-tables',
    children: [
      {
        title: 'Smart Table',
        link: '/pages/tables/smart-table',
      },
    ],
  },
  {
    title: 'Miscellaneous',
    icon: 'nb-shuffle',
    children: [
      {
        title: '404',
        link: '/pages/miscellaneous/404',
      },
    ],
  },
  {
    title: 'Auth',
    icon: 'nb-locked',
    children: [
      {
        title: 'Login',
        link: '/auth/login',
      },
      {
        title: 'Register',
        link: '/auth/register',
      },
      {
        title: 'Request Password',
        link: '/auth/request-password',
      },
      {
        title: 'Reset Password',
        link: '/auth/reset-password',
      },
    ],
  },
];
