import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SpecializationComponent } from './specialization.component';
import { DoctorsComponent } from './doctors/doctors.component';
import { PatientsComponent } from './patients/patients.component';

const routes: Routes = [{
  path: '',
  component: SpecializationComponent,
  children: [{
    path: 'tinymce',
    component: DoctorsComponent,
  }, {
    path: 'ckeditor',
    component: PatientsComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SpecializationRoutingModule { }

export const routedComponents = [
  SpecializationComponent,
  DoctorsComponent,
  PatientsComponent,
];
