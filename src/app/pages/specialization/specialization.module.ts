import { NgModule } from '@angular/core';
import { CKEditorModule } from 'ng2-ckeditor';

import { ThemeModule } from '../../@theme/theme.module';

import { SpecializationRoutingModule, routedComponents } from './specialization-routing.module';

@NgModule({
  imports: [
    ThemeModule,
    SpecializationRoutingModule,
    CKEditorModule,
  ],
  declarations: [
    ...routedComponents,
  ],
})
export class SpecializationModule { }
